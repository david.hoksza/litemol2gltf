import '../styles/index.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'litemol/dist/css/LiteMol-plugin.css';


import {GLTFExporter} from './GLTFExporter';
import LiteMol from 'litemol/dist/js/LiteMol-plugin';
import {init as appInit} from './LiteMol-example';


import $ from 'jquery';
import * as download from 'downloadjs';

window.$ = $;

console.log('LiteMol',LiteMol);

LiteMol.Visualization.THREE.Mesh.prototype.isMesh = true;
LiteMol.Visualization.THREE.Mesh.prototype.drawMode = 0; //TrianglesDrawMode;

LiteMol.Visualization.THREE.SkinnedMesh.prototype.isSkinnedMesh = true;
LiteMol.Visualization.THREE.MeshBasicMaterial.prototype.isMeshBasicMaterial = true;
LiteMol.Visualization.THREE.LineBasicMaterial.prototype.isLineBasicMaterial = true;
LiteMol.Visualization.THREE.ShaderMaterial.prototype.isShaderMaterial = true;


LiteMol.Visualization.THREE.MeshPhongMaterial.prototype.userData = {};
LiteMol.Visualization.THREE.MeshPhongMaterial.prototype.emissiveIntensity = 1;

LiteMol.Visualization.THREE.BufferGeometry.prototype.isBufferGeometry = true;
LiteMol.Visualization.THREE.BufferGeometry.prototype.index = null;
LiteMol.Visualization.THREE.BufferGeometry.prototype.drawRange = { start: 0, count: Infinity };
LiteMol.Visualization.THREE.BufferGeometry.prototype.userData = {};

LiteMol.Visualization.THREE.Line.prototype.isLine = true;

LiteMol.Visualization.THREE.Camera.prototype.isCamera = true;
LiteMol.Visualization.THREE.OrthographicCamera.prototype.isOrthographicCamera = true;

LiteMol.Visualization.THREE.Light.prototype.isLight = true;
LiteMol.Visualization.THREE.DirectionalLight.prototype.isDirectionalLight = true;
LiteMol.Visualization.THREE.PointLight.prototype.isPointLight = true;
LiteMol.Visualization.THREE.SpotLight.prototype.isSpotLight = true;



document.addEventListener("DOMContentLoaded", function () {
    if (window.LiteMol == undefined){
        window.LiteMol = LiteMol;

        let inputHeight = $('#containerInput').css('height');
        let exportHeight = $('#containerExport').css('height');
        console.log(inputHeight, exportHeight);
        $('#viewport').css('height', `calc(100vh - ${inputHeight} - ${inputHeight} - 40px)`);


        $('#btnSubmitPdbId').on('click', ()=>reInit());
        // init('1crn');
        // appInit('3q27', 'viewport');
        //init('3q27');
        //
    }
});

function reInit(){
    window.pdbId = $('#pdbId').val();

    if (window.plugin) {
        window.plugin.destroy();
        window.plugin = undefined;
    }
    appInit(window.pdbId, 'viewport');
    $('#btnExport').off();
    $('#btnExport').on('click',() => exportToGlTF());
    return false;
}

function  init(pdbId){
    window.plugin = LiteMol.Plugin.create({ target: '#viewport' });
    _plugin.loadMolecule({
        id: pdbId,
        url: `https://www.ebi.ac.uk/pdbe/static/entry/${pdbId}_updated.cif`,
        format: 'cif' // default
    });
    console.log('plugin', _plugin);
}

const exportToGlTF = function(){

    // new GLTFExporter().parse( _stage.viewer.scene, function ( gltf ) {
    new GLTFExporter().parse( window.plugin._instance.context.scene.scene.scene, function ( gltf ) {
        console.log( gltf );
        download(JSON.stringify(gltf, null, 4), `${window.pdbId}.gltf`, "text/json");
        // NGL.download(new Blob([JSON.stringify(gltf)], {type : "application/json"}), `${_pdbId}.gltf`
        // );
        // downloadJSON( gltf );
    }, {onlyVisible:true});

    // new ColladaExporter().parse( _stage.viewer.scene, function ( data ) {
    //     console.log( data );
    //     NGL.download(new Blob([data.data], {type : "application/xml"}), `${_pdbId}.dae`);
    //     // downloadJSON( gltf );
    // });
};
