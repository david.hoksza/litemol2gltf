# litemol2gltf 

Litemol2glTF exports scenes from LiteMol to the glTF format.

The project uses the [LiteMol](https://litemol.org) viewer to download, visualize 
and adjust the look of a molecule which is provided by a PDB ID. 

The visualization is provided by a modified LiteMol's CustomControles [example](https://github.com/dsehnal/LiteMol/tree/master/examples).


The Export functionality
is provided by a modified version of [GLTFExporter](https://threejs.org/docs/#examples/exporters/GLTFExporter) 
from the [three.js](http://threejs.org) project.  litemol2gltf uses a modified exporter because
LiteMol is built over a modified old release of three.js which is not compatible with the current
GLTFExporter.

### Installation

```
npm install
```
and
```
npm build
```
or
```
npm start
```

### Current limitations
- transparency
- indexed buffer attributes (see three.js and glTF specification) are unfolded, because
the glTFExporter does not currently support them

